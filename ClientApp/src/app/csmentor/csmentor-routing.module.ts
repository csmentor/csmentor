import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { CoursesComponent } from './courses/courses.component';
import { FourmComponent } from './fourm/fourm.component';
import { InternshipComponent } from './internship/internship.component';
import { JobPortalComponent } from './job-portal/job-portal.component';
import { LandingPageComponent } from './landing-page/landing-page.component';

const routes: Routes = [
  {path:'',component:LandingPageComponent},
  {path:'landing',component:LandingPageComponent},
  {path:'courses',component:CoursesComponent},
  {path:'internship',component:InternshipComponent},
  {path:'apply-jobs',component:JobPortalComponent},
  {path:'q&a-fourm',component:FourmComponent},
  {path:'about-us',component:AboutUsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CsMentorRoutingModule { }
