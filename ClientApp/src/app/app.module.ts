import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CsMentorModule } from './csmentor/csmentor.module';
import { FooterComponent } from './csmentor/footer/footer.component';
import { HeaderComponent } from './csmentor/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CsMentorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
