import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isMobileView:boolean=false;
  isShowHeaderDropdown:boolean=false;
  @HostListener('window:resize', ['$event'])
onResize() {
  if(window.innerWidth<=850){
      this.isMobileView=true;
    }
    else{
      this.isMobileView=false;
    }
}
  constructor() { }

  ngOnInit(): void {
    
  }
  open_User_Dropdown(){
    this.isShowHeaderDropdown=!this.isShowHeaderDropdown
  }

}
