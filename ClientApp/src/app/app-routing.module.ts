import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CsMentorRoutingModule } from './csmentor/csmentor-routing.module';

const routes: Routes = [
  {path:'',redirectTo:'landing',pathMatch:'full'},
  {path:'**',redirectTo:'landing',pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      relativeLinkResolution:"legacy",
      scrollPositionRestoration: "enabled"
    }),CsMentorRoutingModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
