import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { AboutUsComponent } from './about-us/about-us.component';
import { BootstrapComponent } from './courses/bootstrap/bootstrap.component';
import { CssComponent } from './courses/css/css.component';
import { DeepLearningComponent } from './courses/deep-learning/deep-learning.component';
import { HtmlComponent } from './courses/html/html.component';
import { MachineLearningComponent } from './courses/machine-learning/machine-learning.component';
import { PythonComponent } from './courses/python/python.component';
import { CsmentorComponent } from './csmentor.component';
import { InternshipComponent } from './internship/internship.component';
import { JobPortalComponent } from './job-portal/job-portal.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { BannerComponent } from './landing-page/banner/banner.component';
import { CoursesComponent } from './courses/courses.component';
import { FourmComponent } from './fourm/fourm.component';


@NgModule({
  declarations: [
    CsmentorComponent,
    LandingPageComponent,
    InternshipComponent,
    AboutUsComponent,
    JobPortalComponent,
    PythonComponent,
    MachineLearningComponent,
    DeepLearningComponent,
    HtmlComponent,
    CssComponent,
    BootstrapComponent,
    BannerComponent,
    CoursesComponent,
    FourmComponent,
  ],
  imports:[BrowserModule,AppRoutingModule],
  exports: [CsmentorComponent],
  providers: [],
})
export class CsMentorModule { }
