import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CsmentorComponent } from './csmentor.component';

describe('CsmentorComponent', () => {
  let component: CsmentorComponent;
  let fixture: ComponentFixture<CsmentorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CsmentorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CsmentorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
